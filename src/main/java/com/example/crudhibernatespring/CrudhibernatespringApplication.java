package com.example.crudhibernatespring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudhibernatespringApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudhibernatespringApplication.class, args);
    }

}
