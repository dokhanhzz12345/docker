package com.example.crudhibernatespring.controller;

import com.example.crudhibernatespring.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.crudhibernatespring.bean.*;

import java.util.List;


@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @PostMapping("/addBook")
    public Book addBook(@RequestBody Book book) {
        return bookService.addBook(book);
    }

    @PostMapping("/addBooks")
    public List<Book> addBooks(@RequestBody List<Book> books) {
        return bookService.addBooks(books);
    }

    @GetMapping("/getBookById/{id}")
    public Book getBook(@PathVariable int id) {
        return bookService.getBookById(id);
    }

    @DeleteMapping("/DeleteById/{id}")
    public String deleteBook(@PathVariable int id) {
        return bookService.deleteBook(id);
    }

    @PutMapping("/update")
    public Book updateBook(@RequestBody Book book) {
        return bookService.updateBook(book);
    }

    @GetMapping("/getAllBooks")
    public List<Book> listBooks() {
        return bookService.getBooks();
    }
}
