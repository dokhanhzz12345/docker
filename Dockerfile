FROM openjdk:8
EXPOSE 8080
ADD target/CrudHibernateSpringDocker.jar CrudHibernateSpringDocker.jar 
ENTRYPOINT ["java","-jar","/CrudHibernateSpringDocker.jar"]